<?php 
    include("Includes/Header.php"); 
    include("Class/ClassCrud.php");
?>

<div class="content">

    <table class="tabela-crud">
        <tr>
            <td>Nome</td>
            <td>Sexo</td>
            <td>Cidade</td>
            <td>Ações</td>
        </tr>

        <!-- Estrutura de loop -->
        <?php
            $crud = new ClassCrud();
            $beforeFetch = $crud->select("*", "cadastro", "", "", array());
            while($result = $beforeFetch->fetch_all()){
                foreach($result as $fetch) {
        ?>
        <tr>
            <td>
                <?php echo $fetch[1]; ?>
            </td>
            <td>
                <?php echo $fetch[2]; ?>
            </td>
            <td>
                <?php echo $fetch[3]; ?>
            </td>
            <td>
                <a href="visualizar.php<?php echo '?id='.$fetch[0] ?>">
                    <i class="material-icons">search</i>
                </a>
                <a href="cadastro.php<?php echo '?id='.$fetch[0] ?>">
                    <i class="material-icons">edit</i>
                </a>
                <a class="deletar" href="Controllers/ControllerDeletar.php<?php echo '?id='.$fetch[0] ?>">
                    <i class="material-icons">delete</i>
                </a>
            </td>
        </tr>
        <?php
                }
            }
        ?>
    </table>

</div>

<script src="main.js"></script>
    
<?php include("Includes/Footer.php"); ?>
