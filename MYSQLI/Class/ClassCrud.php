<?php
include("ClassConexao.php");
class ClassCrud extends ClassConexao {

    private $crud;
    private $contador;
    private $resultado;

    private function preparedStatements($query, $tipos, $parametros) {
        $this->countParametros($parametros);
        $conexao = $this->conectaDB();
        $this->crud = $conexao->prepare($query);

        if ($this->contador > 0) {
            $callParameters = array();
            foreach ($parametros as $key => $parametro) {
                $callParameters[$key] = &$parametros[$key];
            }
            array_unshift($callParameters, $tipos);
            call_user_func_array(array($this->crud, 'bind_param'), $callParameters);
        }

        $this->crud->execute();
        $this->resultado = $this->crud->get_result();
    }

    private function countParametros($parametros) {
        $this->contador = count($parametros);
    }  

    public function insert($tabela, $condicao, $tipos, $parametros) {
        $this->preparedStatements("INSERT INTO {$tabela} VALUES ({$condicao})", $tipos, $parametros);
        return $this->crud;
    }

    public function select($campos, $tabela, $condicao, $tipos, $parametros) {
        $this->preparedStatements("SELECT {$campos} FROM {$tabela} {$condicao}", $tipos, $parametros);
        return $this->resultado;
    }

    public function delete($tabela, $condicao, $tipos, $parametros) {
        $this->preparedStatements("DELETE FROM {$tabela} WHERE {$condicao}", $tipos, $parametros);
        return $this->crud;
    }

    public function update($tabela, $set, $condicao, $tipos, $parametros) {
        $this->preparedStatements("UPDATE {$tabela} SET {$set} WHERE {$condicao}", $tipos, $parametros);
        return $this->crud;
    }
}