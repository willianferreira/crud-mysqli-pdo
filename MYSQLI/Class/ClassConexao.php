<?php

// Abstrata porque só pode ser extendida
abstract class ClassConexao {

    // Realizará a conexção com o banco
    protected function conectaDB() {
        try {
            $conexao = new mysqli("localhost", "root", "", "crud");
            return $conexao;
        } catch (PDOException $erro) {
            return $erro->getMessage();
        }
    }
}