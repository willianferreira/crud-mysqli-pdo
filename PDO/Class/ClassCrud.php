<?php 
include("ClassConexao.php");
class ClassCrud extends ClassConexao {
    
    private $crud;
    private $contador;  

    private function preparedStatements($query, $parametros) {
        $this->countParametros($parametros);
        $this->crud = $this->ConectaDB()->prepare($query);
        if ($this->contador > 0) {
            for($i = 0; $i <= $this->contador; $i++) {
                $this->crud->bindValue($i, $parametros[$i - 1]);
            }
        }
        $this->crud->execute();
    }

    private function countParametros($parametros) {
        $this->contador = count($parametros);
    }  

    public function insert($tabela, $condicao, $parametros) {
        $this->preparedStatements("INSERT INTO {$tabela} VALUES ({$condicao})", $parametros);
        return $this->crud;
    }

    public function select($campos, $tabela, $condicao, $parametros) {
        $this->preparedStatements("SELECT {$campos} FROM {$tabela} {$condicao}", $parametros);
        return $this->crud;
    }

    public function delete($tabela, $condicao, $parametros) {
        $this->preparedStatements("DELETE FROM {$tabela} WHERE {$condicao}", $parametros);
        return $this->crud;
    }

    public function update($tabela, $set, $condicao, $parametros) {
        $this->preparedStatements("UPDATE {$tabela} SET {$set} WHERE {$condicao}", $parametros);
        return $this->crud;
    }
}


